# Tick Tack

Countdown made with Vue.js 3. Make the most simple, easy and ready to use countdown.

**Table of Contents:**

- [Purpose](#purpose)
- [Features](#features)
- [Issues](#issues)
- [Contributions](#contributions)
- [Project setup](#project-setup)
  - [Local compilation](#compiles-and-hot-reloads-for-development)
  - [Compile for production](#compiles-and-minifies-for-production)
  - [Test](#run-your-unit-tests)
  - [Lint](#lints-and-fixes-files)
  - [Costumization](#customize-configuration)

## Purpose

Show what can I do with Vue 3 and his new features, like composition api and Typescript support.

## Features

- Start a timer. Easy, right?

  ![main page example](doc/main_page.png "Main page")

## Issues

For any issue or problem using this project, open an issue and I'll get in as soon as possible for me. Remember that this is a curriculum project, not the objective of my life. Be patient.

## Contributions

If you are interested in this project, you are free to fork it or open pull requests to contribute.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your unit tests

```
npm run test:unit
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Collaborate with your team

- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:4c9d250b06f2b8f868627791e46c7fa5?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:4c9d250b06f2b8f868627791e46c7fa5?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:4c9d250b06f2b8f868627791e46c7fa5?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:4c9d250b06f2b8f868627791e46c7fa5?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:4c9d250b06f2b8f868627791e46c7fa5?https://docs.gitlab.com/ee/user/clusters/agent/)

---

## Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Roadmap

If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.
