import { Store } from "vuex";

declare module "@vue/runtime-core" {
  interface State {
    timer: number;
  }

  interface ComponentCustomProperties {
    $store: Store<State>;
  }
}
